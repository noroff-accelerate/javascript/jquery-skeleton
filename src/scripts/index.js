import $ from 'jquery'
import 'bootstrap/dist/css/bootstrap.min.css'
import '../styles/index.scss'

/**
 * Global Constants
 */

/**
 * Variables
 */

/**
 * Wait for the DOM to be ready
 * jQuery needs to cache to DOM :)
 */
$(document).ready(function () {
    // TODO Add your listeners here
})

/**
 * Build a Card template with a picture.
 * @param {string|null} photo
 * @param {string} title
 * @param {string} body
 * @returns String
 */
function createCard(url, title, body) {
    return `
        <div class="col-4">
            <div class="card h-100" style="18rem;">
                ${url
                    ? `
                        <div class="card-img">
                            <img class="card-img-top" src="${url}"/>
                        </div>
                      ` : ''}

                <div class="card-body">
                    <h5>${title}</h5>
                    ${body.split('\n').map(para => `<p>${para}</p>`).join('\n')}
                </div>
            </div>
        </div>
    `;
}